# Sobre

Teste - Angular5 - GigaNet - Luiz Carlos - MongoDB Container

# Instruções

```
    git clone https://luizcarlos_dev@bitbucket.org/luizcarlos_dev/teste_angular_giganet_mongodb_container.git
```

```
    cd teste_angular_giganet_mongodb_container
```

# Ajustar ambiente

```
    cp mongod.conf.example mongod.conf
```

#Executar em um container docker (requer docker compose)

```
    npm run deploy
```
