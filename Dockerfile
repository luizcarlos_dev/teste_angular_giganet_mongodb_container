FROM bitnami/mongodb

# Create app directory
RUN mkdir -p /usr/share/teste_giganet_mongodb_container
WORKDIR /usr/share/teste_giganet_mongodb_container

# Start service
EXPOSE 27017
CMD [ "sh" ]
